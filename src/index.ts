import { serve } from "@hono/node-server";
import { Hono } from "hono";
import admin from "./routes/admin/admin";
import { RegExpRouter } from "hono/router/reg-exp-router";
import { logger } from "hono/logger";
const app = new Hono({ strict: false, router: new RegExpRouter() });

app.use("*", logger());
app.route("/admin", admin);
app.showRoutes();

// for custom 404 page
app.notFound((c) => {
  return c.text("NOT FOUND");
});

serve({ fetch: app.fetch, port: 5000 }, () => {
  console.log("Server running on PORT 5000");
});
