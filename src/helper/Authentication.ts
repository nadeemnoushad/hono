import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { prisma } from "../prisma";

interface OTP {
  otp: string;
  expiry: Date;
}
export default class Authentication {
  model: any;
  jwt_secret: string;
  jwt_expres_in: string;

  constructor(model: any) {
    this.model = model;
    this.jwt_secret = "lol";
    this.jwt_expres_in = process.env.JWT_EXPIRE as string;
  }

  async register(data: any) {
    const user_exist = await this.model.findUnique({
      where: { email: data.email },
    });

    if (user_exist)
      throw new Error("An account is already associated with this email");

    const salt = await bcrypt.genSalt(10);
    const hashed_password = await bcrypt.hash(data.password, salt);

    data.password = hashed_password;
    const user = await this.model.create({
      data: data,
    });
    return user;
  }

  async login(email: string, password: string) {
    console.log(email, password);

    const user = await this.model.findUnique({ where: { email } });
    if (!user) {
      throw new Error("Invalid email or password");
    }

    const isValid = await bcrypt.compare(password, user.password);
    if (!isValid) {
      throw new Error("Invalid email or password");
    }

    const token = jwt.sign(
      { id: user.id, email, name: user.name },
      this.jwt_secret
      // { expiresIn: this.jwt_expres_in } //TODO: uncomment this line
    );

    return token;
  }
  async forgotPassword(email: string) {
    const user = await this.model.findFirst({ where: { email } });
    if (!user) {
      throw new Error("Invalid email");
    }

    // generating otp // TODO: create func to generate
    const otp = "111111";

    // clearing previous otps
    // if (await prisma.seller.findFirst(  )) {
    //   await prisma.otp.delete({ where: { email } });
    // }

    // save otp to db // TODO: add expiry
    const data = {
      otp: otp,
      expires: Date.now(),
    };

    const seller = await prisma.seller.update({
      data: { otp: data },
      where: { email },
    });

    //TODO: send otp to email
    console.log(seller);

    return true;
  }

  // ! CHECK

  async verifyOTP(email: string, otp: string) {
    const seller = await prisma.seller.findFirst({ where: { email } });
    console.log(seller);

    const sellerOTP = seller?.otp as unknown as OTP;

    if (seller && sellerOTP.otp === otp) return true;
    else return false;
  }

  async resetPassword(email: string, otp: string, new_password: string) {
    if (!(await this.verifyOTP(email, otp))) {
      throw new Error("Invalid otp");
    }

    const user = await this.model.findFirst({ where: { email } });
    const hashed_new_password = await bcrypt.hash(new_password, 10);
    console.log(user);

    user.password = hashed_new_password;
    console.log(this.model);

    await Promise.all([
      await this.model.update({
        where: { id: user.id },
        data: { password: hashed_new_password },
      }),
      // prisma.otp.delete({ where: { email } }),
    ]);

    const token = jwt.sign(
      { id: user.id, email, name: user.name },
      this.jwt_secret
    );

    return token;
  }
}
