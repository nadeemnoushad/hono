import { Context } from "hono";
import { prisma } from "../../prisma";
import Authentication from "../../helper/Authentication";
const auth = new Authentication(prisma.seller);

export const login = async (c: Context) => {
  const { body } = c.formatted_req;
  console.log(body);
  const token = await auth.login(body.email, body.password);
  return c.json({ token });
};

export const register = async (ctx: Context) => {
  const body = ctx.req.valid("form");
  console.log(body);

  const user = await auth.register(body);
  return ctx.json(user);
};

export const authRoute = async (c: Context) => {
  console.log("authorized");
  const body = c.req.json();
  // console.log(c.req.headers.get("authorization"));
  console.log(c.req.header("authorization"));
  console.log(c.user);

  return c.json({ msg: "authorized" });
};
