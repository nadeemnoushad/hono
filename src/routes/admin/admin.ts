import { Context, Hono } from "hono";
import { authRoute, login, register } from "./admin.controller";
import { jwt } from "hono/jwt";
import { authorize } from "../../middleware/authorize";
import { validate } from "../../middleware/validate";
import { sellerLoginSchema } from "../../validations";
import { validator } from "hono/validator";
import { writeFile } from "fs";
const admin = new Hono({ strict: false });

admin.post("/login", validate(sellerLoginSchema), login);
admin.post("/register", register);

admin.get("/auth", authorize, authRoute);

admin
  .post("/hi", async (c) => {
    const data = await c.req.json();

    return c.json(data);
  })
  .get((c) => {
    return c.json({ msg: "chained route" });
  });

admin.post(
  "/",
  validator("json", (value, c) => {
    const body = value["name"];
    console.log(body);

    if (!body || typeof body !== "string") {
      return c.text("Invalid!", 400);
    }
    return { body };
  }),
  (c) => {
    const body = c.req.valid("json");
    console.log(body);

    return c.json({ msg: "endpoint using hono validator" });
  }
);

admin.on("PURGE", "/test", (c) => c.text("HEHE Method /test"));

admin.on(["GET", "POST"], "/on", async (c) => {
  if ("GET") {
    const data = await c.req.json();
  }
  return c.json({ msg: "multiple method endpoint" });
});

admin.get("/", (c) => {
  console.log("get endpoint");

  return c.json({ msg: "working" });
});

// file upload
admin.post("/upload", async (c: Context) => {
  const body = await c.req.parseBody();
  console.log(body);

  // writeFile("/upload", body, () => {})
  return c.json({ msg: "upload endpoint" });
});

export default admin;
