import { z } from "zod";

// AUTH VALIDATORS //
const emailValidator = z
  .string({
    required_error: "Email is required",
    invalid_type_error: "Email should be a string",
  })
  .email("Invalid email");

const passwordValidator = z
  .string()
  .min(4, "Password must contain a minimum of 4 characters")
  .max(20, "Password should not contain more than 20 characters");

export const phoneNumberValidator = z
  .string()
  .length(10, "Phone number should contain 10 digits");

const otpValidator = z.string().min(1, "Enter the otp");

// AUTH SCHEMAS //
export const loginSchema = z.object({
  email: emailValidator,
  password: passwordValidator,
});

export const registerSchema = z.object({
  phone: phoneNumberValidator,
  name: z.string().min(1, "Name is required"),
  email: emailValidator,
  password: passwordValidator,
});

export const resetPasswordSchema = z.object({
  email: emailValidator,
  password: passwordValidator,
  otp: otpValidator,
});

export const forgotPasswordSchema = z.object({
  email: emailValidator,
});

export const verifyOTPSchema = z.object({
  email: emailValidator,
  otp: otpValidator,
});
