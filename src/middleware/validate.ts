import { AnyZodObject } from "zod";
import { Context, Next, Hono } from "hono";
import { generateErrorMessage } from "zod-error";

declare module "hono" {
  interface Context {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    formatted_req: any;
  }
}

export const validate =
  (schema: AnyZodObject) => async (c: Context, next: Next) => {
    console.log("validate middleware");

    const result = await schema.safeParseAsync({
      body: await c.req.json(),
      params: c.req.param(),
      query: c.req.query(),
    });

    if (!result.success)
      return c.json({ error: generateErrorMessage(result.error.issues) });

    c["formatted_req"] = result.data;
    await next();
  };
