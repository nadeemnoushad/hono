import { Next, Context, HonoRequest } from "hono";
import jwt, { Secret } from "jsonwebtoken";

const unAuthorizedException = () => {
  throw new Error("Unauthorized");
};

declare module "hono" {
  interface Context {
    user?: any;
  }
}

const decodeJwt = (token: string) => {
  try {
    return jwt.verify(token, "lol");
  } catch (err: any) {
    console.log("jwt decode error", err.message);
    unAuthorizedException();
  }
};

export async function authorize(c: Context, next: Next) {
  const token = c.req.headers.get("authorization");

  if (token) {
    try {
      const decoded = decodeJwt(token);
      c.user = decoded;
      await next();
    } catch (err) {
      c.status(400);
      c.json({ error: "Invalid token" });
    }
  } else {
    c.status(401);
    c.json({ error: "No token provided" });
  }
}
