import { z } from "zod";
import {
  forgotPasswordSchema,
  loginSchema,
  registerSchema,
  resetPasswordSchema,
  verifyOTPSchema,
} from "./auth";

export const sellerLoginSchema = z.object({
  body: loginSchema,
});

export const sellerRegisterSchema = z.object({
  body: registerSchema,
});

export const sellerForgotPasswordSchema = z.object({
  body: forgotPasswordSchema,
});

export const sellerResetPasswordSchema = z.object({
  body: resetPasswordSchema,
});

export const sellerVerifyOTPSchema = z.object({
  body: verifyOTPSchema,
});

export type SellerLogin = z.infer<typeof sellerLoginSchema>;
export type SellerRegister = z.infer<typeof sellerRegisterSchema>;
export type SellerForgotPassword = z.infer<typeof sellerForgotPasswordSchema>;
export type SellerResetPassword = z.infer<typeof sellerResetPasswordSchema>;
export type SellerVerifyOTP = z.infer<typeof sellerVerifyOTPSchema>;
